$('.tabs-list').each(function () {
  // For each set of tabs, we want to keep track of
  // which tab is active and its associated content
  var $active, $content, $links = $(this).find('.tabs-list-link');

  // If the location.hash matches one of the links, use that as the active tab.
  // If no match is found, use the first link as the initial active tab.
  $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
  $active.addClass('is-active');

  $content = $($active[0].hash);

  // Hide the remaining content
  $links.not($active).each(function () {
    $(this.hash).addClass('is-hidden');
  });

  // Bind the click event handler
  $(this).on('click', '.tabs-list-link', function (e) {
    // Make the old tab inactive.
    $active.removeClass('is-active');
    $content.addClass('is-hidden');

    // Update the variables with the new link and content
    $active = $(this);
    $content = $(this.hash);

    // Make the tab active.
    $active.addClass('is-active');
    $content.removeClass('is-hidden');

    // Prevent the anchor's default click action
    e.preventDefault();
  });
});