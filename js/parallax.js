let images;
let imagesList;
let ticking;
let isMobile;
let wasAnimated;

const checkAvailability = function () {
  if (!isMobile) {
    doParallax();
    wasAnimated = true;
  } else if (isMobile && wasAnimated) {
    setDefaultTransform();
    wasAnimated = false;
  }
};

const doParallax = function () {
  let viewportHeight = document.documentElement.clientHeight;
  let bodyScrollTop = document.body.scrollTop;
  let documentScrollTop = document.documentElement.scrollTop;
  let windowScrollTop = bodyScrollTop + documentScrollTop;
  ticking = false;

  images.forEach(function (elem, index) {
    let elementHeight = elem.parentElement.scrollHeight;
    let elementOffsetTop = elem.parentElement.offsetTop;
    let scrolledPercent;
    let isEven;

    //check element position
    scrolledPercent = (windowScrollTop - elementOffsetTop + viewportHeight) / (elementHeight + viewportHeight);

    //check if element is even
    isEven = (index + 1) % 2 == 0;

    //calculate offset based on even/odd elements order
    transformOrigin = calculateTransformOrigin(isEven, scrolledPercent);

    //set transform property
    (scrolledPercent > 0 && scrolledPercent < 1) ? setTransformOrigin(elem, transformOrigin) : "";
  });

  ticking = false;
};

const setDefaultTransform = function () {
  images.forEach(function (element) {
    element.style.transformOrigin = "center";
  });
};

const calculateTransformOrigin = function (isEven, scrolledPercent) {
  const initialOffset = 20;
  let offset = initialOffset * scrolledPercent;

  return isEven ? offset : 100 - offset;
};

const setTransformOrigin = function (el, transform) {
  el.style.transformOrigin = transform + '% 50%';
};

window.addEventListener('DOMContentLoaded', function (e) {
  imagesList = document.querySelectorAll('.has-parallax');
  images = Array.prototype.slice.call(imagesList);
  isMobile = window.innerWidth < 768;
  ticking = false;
  wasAnimated = false;
  checkAvailability();
});

window.addEventListener('scroll', function (e) {
  ticking = true;
  if (ticking) {
    window.requestAnimationFrame(checkAvailability);
  }
});

window.addEventListener('resize', function (e) {
  ticking = true;
  if (ticking) {
    isMobile = window.innerWidth < 768;
    window.requestAnimationFrame(checkAvailability);
  }
});