/**
 * Connects Contact Form
 */
(function ($) {

    $(document).ready(function () {
        var selectors = {};
        var $body = $('body');

        selectors.contact_full_name = '.footer-contact-form .gfield_fullname';
        selectors.contact_phone = '.footer-contact-form .gfield_phone';
        selectors.contact_email = '.footer-contact-form .gfield_email';
        selectors.contact_msg = '.footer-contact-form .gfield_message';

        // Send data event
        $('.footer-contact-form').on('submit', send_contact_to_mc);


        function send_contact_to_mc(event) {
            event.preventDefault();
            var contact_full_name, contact_phone, contact_email, contact_msg;

            var first_name, last_name;

            // Get values from Contact Form
            contact_full_name = $(selectors.contact_full_name + ' input').val();
            contact_phone = $(selectors.contact_phone + ' input').val();
            contact_email = $(selectors.contact_email + ' input').val();
            contact_msg = $(selectors.contact_msg + ' textarea').val();

            // fields are not filled
            var validation_error = false;

            if (typeof contact_full_name === "undefined" || contact_full_name === '') {
                $(selectors.contact_full_name).addClass('gfield_error');
                validation_error = true;
            }
            else {
                $(selectors.contact_full_name).removeClass('gfield_error');
            }

            if (typeof contact_phone === "undefined" || contact_phone === '') {
                $(selectors.contact_phone).addClass('gfield_error');
                validation_error = true;
            }
            else {
                $(selectors.contact_phone).removeClass('gfield_error');
            }

            if (! isEmail(contact_email)) {
                $(selectors.contact_email).addClass('gfield_error');
                validation_error = true;
            }
            else {
                $(selectors.contact_email).removeClass('gfield_error');
            }

            if (typeof contact_msg === "undefined" || contact_msg === '') {
                $(selectors.contact_msg).addClass('gfield_error');
                validation_error = true;
            }
            else {
                $(selectors.contact_msg).removeClass('gfield_error');
            }

            // fields are not filled
            if ( validation_error ) {
                console.log('fields are not filled');
                return false;
            }

            $('.footer-contact-form input[type=submit]').attr("disabled", "disabled");

            $.ajax({
                method: "POST",
                url: "/request-form/index.php",
                data: {
                    name: contact_full_name,
                    phone: contact_phone,
                    email: contact_email,
                    msg: contact_msg,
                    token: "c7b7ded87ab3060cc2d993fa1ba3d759"
                }
            }).done(function( msg ) {
                if ($('.footer-contact-form .success_message').length == 0) {
                    $(".gform_footer").append("<p class='success_message'>Request has been sent. Thank you!</p>");
                }
            }).fail(function( jqXHR, textStatus ) {
                console.log( "Request failed: " + textStatus );
            });

        }

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

    });

}(jQuery));