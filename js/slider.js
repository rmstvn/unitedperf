jQuery(document).ready(function ($) {
  var sectionsAvailable = $('.slide');
  var menuLinks = $('.menu-link');
  var menuBtn = $('.menu-btn');
  var navItems = [];
  var isAnimating = false;
  scrollAnimation();
  initNav();
  initNavBtn();

  $(window).on('scroll', scrollAnimation);

  //set active link, call triggering click method
  function initNav() {
    $.each(menuLinks, function () {
      var item = $(this);
      if (item.data('slide') !== undefined) {
        navItems.push(item);
        bindNavClick(item);
      }
    });
    setActiveNavItem(navItems);
  }

  //set active link, call triggering click method
  function initNavBtn() {
    $.each(menuBtn, function () {
      var item = $(this);
      if (item.data('slide') !== undefined) {
        navItems.push(item);
        bindNavClick(item);
      }
    });
    setActiveNavItem(navItems);
  }

  //bind click on menu item
  function bindNavClick(navItem) {
    navItem.on('click', function (event) {
      event.preventDefault();
      var currentNavItem = $(this).data('slide');
      var targetScroll = sectionsAvailable.filter("[data-slide='" + currentNavItem + "']");

      slideTo(targetScroll);
      $(this).blur();
    });
  }

  //update dots and mobile
  function setActiveNavItem() {
    var currentSlide = sectionsAvailable.filter('.visible');
    var currentSlideNumber = currentSlide.data('slide');

    $.each(navItems, function () {
      var item = $(this);
      item.removeClass('is-active');
      item.parent().removeClass('current-menu-item');
      if (item.data('slide') === currentSlideNumber) {
        item.addClass('is-active');
        item.parent().addClass('current-menu-item');
      }
    });
  }

  //check if requestAnimationFrame is supported
  function scrollAnimation() {
    (!window.requestAnimationFrame) ? animateSection() : window.requestAnimationFrame(animateSection);
  }

  //scrolling effect
  function animateSection() {
    var scrollTop = $(window).scrollTop();
    sectionsAvailable.each(function () {
      var actualBlock = $(this);
      var offset = parseInt(scrollTop) - parseInt(actualBlock.offset().top);
      (offset >= 0 && offset < parseInt(actualBlock.height())) ? actualBlock.addClass('visible') : actualBlock.removeClass('visible');
    });
    setActiveNavItem();
  }

  //animate sliding
  function slideTo(elem) {
    isAnimating = true;
    $('body, html').animate({
      scrollTop: elem.offset().top
    }, 700, 'linear', function () {
      setActiveNavItem();
      isAnimating = false;
    });
  }
});
