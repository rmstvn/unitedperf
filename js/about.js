jQuery(document).ready(function ($) {
  var $aboutSection = $('.about');
  var isVisible = false;

  if (elementIsExist($aboutSection)) {
    init($aboutSection);
  }

  function init(el) {
    checkVisibility(el);
    bindEvents(el);
  }

  function bindEvents(el) {
    $(window).on('scroll', function () {
      checkVisibility(el);
    });

    $(window).on('resize', function () {
      checkVisibility(el);
    });
  }

  function elementIsExist(el) {
    return el.length;
  }

  function elementInViewport(el) {
    //Window Object
    var $window = $(window);
    //the top Scroll Position in the page
    var scrollTopPosition = $window.scrollTop();
    //the height of the page
    var windowHeight = $window.innerHeight();
    //the end of the visible area in the page, starting from the scroll position
    var visibleAreaEndPoint = scrollTopPosition + windowHeight;
    //the distance from top to the object
    var objTopPosition = el.offset().top;
    //the height of the object
    var objHeight = el.innerHeight();
    //the end point of the object
    var objEndPoint = objTopPosition + objHeight;

    return visibleAreaEndPoint >= objEndPoint && scrollTopPosition <= objTopPosition ? true : false;
  }

  function checkVisibility(el) {
    //check if element is in visible area and haven't animated yet
    if (elementInViewport(el) && !isVisible) {
      //make element visible and change visibility flag
      el.addClass('is-visible');
      isVisible = true;
    }
  }
});
