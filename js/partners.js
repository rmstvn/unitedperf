$('.partners').slick({
  vertical: true,
  verticalSwiping: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  dots: false,
  autoplay: true,
  infinity: true,
  speed: 0,
  autoplaySpeed: 3000
});