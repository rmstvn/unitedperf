jQuery(document).ready(function($) {
  var $menuToggle = $(".js-toggle-menu");
  var $nav = $(".mobile-menu");
  var $header = $(".header");
  var isOpen = false;
  var isMobile = false;
  var isScrollDown = true;
  var isScrollUp = true;
  var prevScrollpos = window.pageYOffset;

  function checkMobile() {
    var windowWidth = $(window).innerWidth();
    return windowWidth < 768 ? true : false;
  }

  function hideMobileMenu() {
    var currentScrollPos = window.pageYOffset;  
    if (checkMobile()) {
      if (prevScrollpos > currentScrollPos && isScrollUp) {
        isScrollUp = false;
        isScrollDown = true;
        $header.css("top", "0");
      } else if(prevScrollpos < currentScrollPos && isScrollDown){
        isScrollUp = true;
        isScrollDown = false;
        $header.css("top", "-100px");
      }

    }
    prevScrollpos = currentScrollPos;
  }

  function toggleMenu() {
    isOpen ? closeMenu() : openMenu();
  }

  function openMenu() {
    $nav.addClass("is-open");
    bodyScrollLock.disableBodyScroll($nav);
    isOpen = true;
  }

  function closeMenu() {
    $nav.removeClass("is-open");
    bodyScrollLock.enableBodyScroll($nav);
    isOpen = false;
  }

  $($nav).on("click", ".menu-item", toggleMenu)

  $($menuToggle).on("click", toggleMenu);

  $(window).on("scroll", _.throttle(hideMobileMenu, 200));

  $(window).on("resize", function(e) {
    isMobile = checkMobile();
    isOpen && !isMobile ? closeMenu() : "";
  });
});
