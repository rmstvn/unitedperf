// Detect objectFit support
if('objectFit' in document.documentElement.style === false) {

  // Loop through HTMLCollection
  const nodeList = document.querySelectorAll('img');
  const imageArr = Array.prototype.slice.call(nodeList);

  // Asign image source to variable
  imageArr.forEach(function (element, index) {
    let backgroundSize = getComputedStyle(element).backgroundSize;
    if (backgroundSize === 'contain' || backgroundSize === 'cover') {
      let path = element.src;
      let className = element.className;
      const classNameForParallax = 'has-parallax';
      let thumb = element.parentElement;
      let pseudoImg = document.createElement("div");

      element.classList.remove(classNameForParallax);
      pseudoImg.className += className;
      thumb.appendChild(pseudoImg);

      // Hide image visually
      element.style.opacity = '0';
      element.style.zIndex = '1';

      // Add background-image: and put image source here
      pseudoImg.style.backgroundImage = 'url(' + path + ')';

      // Add background-size: cover
      pseudoImg.style.backgroundSize = backgroundSize;
    }
  });
}
else {
  // You don't have to worry
  console.log('No worries, your browser supports objectFit')
}