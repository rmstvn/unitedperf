jQuery(document).ready(function ($) {
  var $banner = $('.hero');
  var $header = $('.header');
  var headerHeight = 0;
  var isInvisible = false;
  var isSticky = false;

  if (elementIsExist($banner)) {
    init($banner, $header);
  }

  function init(parent, el) {
    headerHeight = parseInt(el.css("min-height"));
    checkScrollTop(parent, el);
    bindEvents(parent, el);
  }

  function bindEvents(parent, el) {
    $(window).on('scroll', function () {
      checkScrollTop(parent, el);
    });

    $(window).on('resize', function () {
      headerHeight = parseInt(el.css("min-height"));
      checkScrollTop(parent, el);
    });
  }

  function elementIsExist(el) {
    return el.length;
  }

  function checkScrollTop(parent, el) {
    var scrollTop = $(window).scrollTop();
    var bannerHeight = parent.innerHeight();

    if (scrollTop > headerHeight && scrollTop < bannerHeight - headerHeight) {
      isInvisible = true;
    } else {
      isInvisible = false;
    }
    if (scrollTop > bannerHeight - headerHeight) {
      isSticky = true;
    } else {
      isSticky = false;
    }
    toggleInvisible(el);
    toggleSticky(el);
  }

  function toggleSticky(el) {
    isSticky ? el.addClass('is-sticky') : el.removeClass('is-sticky');
  }

  function toggleInvisible(el) {
    isInvisible ? el.addClass('is-invisible') : el.removeClass('is-invisible');
  }
});
